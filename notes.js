// [SECTION] Creating React Application:
	// Syntax:
        npx create - react - app < project - name >

        // Delete unecesary files from the app
        // application > src
        App.test.js
    index.css
    logo.svg
    reportWebVitals.js
    
    // Remove the importation of the "index.css" and "reportWebVitals" files from the "index.js" file. Also remove the code using the reportWebVitals function.
    // Application > src > index.js
    
    // Remove the importation of the "logo.svg" file and most of the codes found inside the "App" component to remove any errors.
    // Application > src > App.js
    
    /*
        The syntax used in Reactjs is JSX.
    
            - JSX - Javascript + XML, It is an extension of Javscript that let's us create objects which will then be compiled and added as HTML elements.
    
            - With JSX, we are able to create HTML elements using JS.
    
            - With JSX, we are able to create JS objects that will then be compiled and added as HTML elements.
    
    */
    
    /* 
    ReactJS Component
    
    this are reusable parts of our react application
    they are independent UI parts of our app.
    Components are functions that return react elements
    components naming convention: PascalCase
    -Capitalized letter for all words of the function name AND file name associated with it
    
    
    // React Bootstrap Components
    SYNTAX
    import {moduleName/s} from "file path"
    
    */
    
    /*
        React.StrictMode is a built-in react component which is used to highlight potential problems in our code and in fact allows for more information about errors in our code.
    */
    
    /* 
    React import pattern:
    -imports from built-in react modules.
    -imports from downloaded packges
    -imports from user defined components
    */
    
    /* 
    Props
    is a shorthand for "property" since components are considered as objects in ReactJS
    is a way to pass data from a parent component to a child component
    it is synonymous to function parameters
    it is used like an HTML attribute added to the child component
    
    Hooks
    -Special / react-defined methods and functions that allow us to do certain tasks in our components
    -Use the state hook for this component to be able to store its state
    */
    
    