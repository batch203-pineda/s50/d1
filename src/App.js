import {Container} from "react-bootstrap";

import Home from "./pages/Home";

import Courses from "./pages/Courses";

import './App.css';

import AppNavBar from "./components/AppNavbar";


/* 
all other components/pages will be contained in our main (parent) component: <App />
*/

/* 
We use React fragment (<>...</>) to render adjacent 
*/
function App() {
  return (
   <>
      <AppNavBar />,
      <Container fluid>
       <Home />
       <Courses />
      </Container>
   </>
  );
}

export default App;
