import {useState} from "react";

import {Card, Button} from "react-bootstrap";
                            //destructure the courseProp from the prop parameter
export default function CourseCard({courseProp})
{
    // console.log(props.courseProp.name);
    // console.log(typeof props);
   //console.log(courseProp);

//Scenario: Keep track the number of enrollees of each course.

// Destructure the course properties into their own variables
    const {name, description, price} = courseProp;

    // syntax
    // const [stateName, setStateName] = useState(initialStateValue);
    // using the state hook, it returns an array with the following elemnts:
        // first element contains the current initial state value 
        // second element is a function that is used to change the first element value of the first element
        
        const [count, setCount] = useState(0);
        const [seat, setSeat] = useState(30);
        //console.log(useState);

        // Function that keeps track of the enrollees for a course.
        function enroll() {
            // 0 + 1
            // setCount(1)
            console.log(`Enrollees: ${count}`);
            
            if (seat == 0 ) {
            
                alert("No more seats.")                
            } else {
                setSeat(seat - 1);
                console.log(`Seat: ${seat}`);
            setCount(count + 1);
            }
            
        }

    return(

        <Card className="my-3">
            <Card.Body>
                <Card.Title>
                    {name}
                </Card.Title>
                    <Card.Subtitle>
                        Description
                    </Card.Subtitle>
                        <Card.Text>
                            {description}
                        </Card.Text>
                <Card.Subtitle>
                    Price:
                </Card.Subtitle>
                <Card.Text>
                    Php {price}
                </Card.Text>
                <Card.Subtitle>
                    Enrollees: 
                </Card.Subtitle>
                <Card.Text>
                    {count} Enrollees
                </Card.Text>
                <Button variant="primary" onClick={enroll}>Enroll Now</Button>
            </Card.Body>
        </Card>
    )
}